package com.sdnc.trade.infrastructure.dao.area;

import java.util.List;

import org.beetl.sql.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import com.sdnc.trade.domain.persistantobject.area.AreaPO;
import com.sdnc.trade.domain.valueobject.area.AreaViewVO;

/**
 *
 * 行政区划表
 *
 *
 */
@Repository
public interface AreaDao extends BaseMapper<AreaPO> {

	default AreaViewVO view(Integer code){
		List<AreaViewVO> list = this.createLambdaQuery()
				.andEq(AreaPO::getCode, code)
				.select(AreaViewVO.class);

		return list.get(0);
	}

}
