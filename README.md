# cqrs-tpl

#### 介绍

基于 CQRS 思想的微服务项目模版

#### 软件架构

1. Java-v17
2. Gradle-v8
3. SpringBoot-v3
4. BeetlSQL-v3
5. Jedis-v4

#### 软件分包

1. application(应用层): 组织业务场景, 编排业务, 隔离场景对领域层的差异
2. domain(领域层): 实现具体的业务逻辑和规则
3. infrastructure(基础设施层): 提供具体的技术实现, 比如存储, 基础设施对业务保持透明
4. interfaces(用户界面层): 向用户显示信息, 处理用户输入
5. 以模块划分微服务, 便于添加或修改代码
6. ~~此项目只是做了一些简单的分层, 完整分层可根据下面的文档进行调整~~
7. 可在 GenerateJavaFileUtils.java 文件中的 main 方法修改自动生成的表名和包名

#### 测试SQL

```sql
-- 导出 fresh-test 的数据库结构
DROP DATABASE IF EXISTS `fresh-test`;
CREATE DATABASE IF NOT EXISTS `fresh-test` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `fresh-test`;
-- 导出  表 fresh-test.area 结构
DROP TABLE IF EXISTS `area`;
CREATE TABLE IF NOT EXISTS `area`
(
    `code`         mediumint(8) unsigned NOT NULL COMMENT '编码',
    `name`         varchar(10)           DEFAULT NULL COMMENT '名称',
    `parent_code`  mediumint(8) unsigned DEFAULT NULL COMMENT '父编码(0:省 其他:市县)',
    `full_name`    varchar(20)           DEFAULT NULL COMMENT '完整名称',
    `type`         tinyint(1) unsigned   DEFAULT NULL COMMENT '类型(1:省 2:市 3:县)',
    `lon`          decimal(9, 6)         DEFAULT NULL COMMENT '经度',
    `lat`          decimal(8, 6)         DEFAULT NULL COMMENT '纬度',
    `sort`         tinyint(3) unsigned   DEFAULT NULL COMMENT '排序',
    `weather_code` int(10) unsigned      DEFAULT NULL COMMENT '天气网编码',
    PRIMARY KEY (`code`) USING BTREE
) ENGINE = Aria
  DEFAULT CHARSET = utf8 PAGE_CHECKSUM=1 COMMENT='行政区划表';
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (110000, '北京', 0, '北京', 1, 0.000000, 0.000000, 1, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (120000, '天津', 0, '天津', 1, 0.000000, 0.000000, 3, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (130000, '河北', 0, '河北', 1, 0.000000, 0.000000, 9, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (140000, '山西', 0, '山西', 1, 0.000000, 0.000000, 10, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (150000, '内蒙古', 0, '内蒙古', 1, 0.000000, 0.000000, 8, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (210000, '辽宁', 0, '辽宁', 1, 0.000000, 0.000000, 7, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (220000, '吉林', 0, '吉林', 1, 0.000000, 0.000000, 6, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (230000, '黑龙江', 0, '黑龙江', 1, 0.000000, 0.000000, 5, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (310000, '上海', 0, '上海', 1, 0.000000, 0.000000, 2, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (320000, '江苏', 0, '江苏', 1, 0.000000, 0.000000, 19, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (330000, '浙江', 0, '浙江', 1, 0.000000, 0.000000, 21, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (340000, '安徽', 0, '安徽', 1, 0.000000, 0.000000, 22, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (350000, '福建', 0, '福建', 1, 0.000000, 0.000000, 23, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (360000, '江西', 0, '江西', 1, 0.000000, 0.000000, 24, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (370000, '山东', 0, '山东', 1, 0.000000, 0.000000, 12, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (410000, '河南', 0, '河南', 1, 0.000000, 0.000000, 18, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (420000, '湖北', 0, '湖北', 1, 0.000000, 0.000000, 20, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (430000, '湖南', 0, '湖南', 1, 0.000000, 0.000000, 25, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (440000, '广东', 0, '广东', 1, 0.000000, 0.000000, 28, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (450000, '广西', 0, '广西', 1, 0.000000, 0.000000, 30, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (460000, '海南', 0, '海南', 1, 0.000000, 0.000000, 31, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (500000, '重庆', 0, '重庆', 1, 0.000000, 0.000000, 4, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (510000, '四川', 0, '四川', 1, 0.000000, 0.000000, 27, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (520000, '贵州', 0, '贵州', 1, 0.000000, 0.000000, 26, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (530000, '云南', 0, '云南', 1, 0.000000, 0.000000, 29, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (540000, '西藏', 0, '西藏', 1, 0.000000, 0.000000, 14, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (610000, '陕西', 0, '陕西', 1, 0.000000, 0.000000, 11, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (620000, '甘肃', 0, '甘肃', 1, 0.000000, 0.000000, 16, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (630000, '青海', 0, '青海', 1, 0.000000, 0.000000, 15, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (640000, '宁夏', 0, '宁夏', 1, 0.000000, 0.000000, 17, 0);
INSERT INTO `area` (`code`, `name`, `parent_code`, `full_name`, `type`, `lon`, `lat`, `sort`, `weather_code`)
VALUES (650000, '新疆', 0, '新疆', 1, 0.000000, 0.000000, 13, 0);
```

#### 打包命令

gradle build -Dfile=test

#### 参考文档

01. [聊一聊 DDD 应用的代码结构](https://wenku.baidu.com/view/43f5184b26c52cc58bd63186bceb19e8b8f6ecfa.html?_wkts_=1674267739736)
02. [DDD 目录结构](https://wenku.baidu.com/view/451bec6af4ec4afe04a1b0717fd5360cbb1a8d57?aggId=43f5184b26c52cc58bd63186bceb19e8b8f6ecfa&_wkts_=1674269092912)
03. [SOFA 应用架构详解](https://mp.weixin.qq.com/s/bYuwxg43FqFKjGv3G83fIw)
04. [SpringBoot 工程分层实战](https://mp.weixin.qq.com/s/1O4XnZ1z4pHluzn0F9i2pg)
05. [凤凰架构](https://icyfenix.cn/)
06. [Java 全栈知识体系](https://pdai.tech/)
07. [SpringBoot 内置工具类](https://mp.weixin.qq.com/s/mFovEO3jQGGP2e6TaFxuXg)
08. [Redis 命令补充](https://github.com/iyayu/RedisUtil)
09. [请求日志组件](https://mp.weixin.qq.com/s/HQPa_XphTFRYTFU30xzk9A)
10. [通过注解自动生成 Spring 配置文件](https://mp.weixin.qq.com/s/VO39ZN_7uO1srMuHFxu9hQ)
11. [如何做一个好的程序员](https://braydie.gitbooks.io/how-to-be-a-programmer/content/zh/)
12. [Java知识](https://www.didispace.com/timeline)
13. [推荐程序员必知的四大神级学习网站](https://mp.weixin.qq.com/s/SuaNYKEAxaTZ0MiH34Iw9w)
14. [高级Java面试题](https://doocs.gitee.io/advanced-java)
15. [常用框架源码分析](https://doocs.gitee.io/source-code-hunter)
