package com.sdnc.common.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * 表示调用的方法（或类中的所有方法）的结果是可以被缓存的。
 * 当该方法被调用时先检查缓存是否命中，如果没有命中再调用被缓存的方法，
 * 并将其返回值放到缓存中。
 *
 * @author Ray
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Cacheable {

	/**
	 * 别名是 {@link #cacheNames}.
	 *
	 * @return String[]
	 */
	@AliasFor("cacheNames")
	String[] value() default {};

	/**
	 * 缓存名称
	 *
	 * @return String[]
	 */
	@AliasFor("value")
	String[] cacheNames() default {};

	/**
	 * 缓存key，支持Beetl表达式
	 *
	 * @return String
	 */
	String key() default "";

	/**
	 * 过期时间
	 *
	 * @return long
	 */
	long timeout() default 6;

	/**
	 * 过期时间单位
	 *
	 * @return long
	 */
	TimeUnit timeUnit() default TimeUnit.HOURS;

}
