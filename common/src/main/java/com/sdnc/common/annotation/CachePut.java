package com.sdnc.common.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 将对应数据放到缓存中
 *
 * @author Ray
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CachePut {

	/**
	 * 别名 {@link #cacheNames}.
	 *
	 * @return String[]
	 */
	@AliasFor("cacheNames")
	String[] value() default {};

	/**
	 * 缓存名称
	 *
	 * @return String[]
	 */
	@AliasFor("value")
	String[] cacheNames() default {};

	/**
	 * 缓存key，支持Beetl表达式
	 *
	 * @return String
	 */
	String key() default "";

}
