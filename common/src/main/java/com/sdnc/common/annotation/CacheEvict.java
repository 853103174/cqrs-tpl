package com.sdnc.common.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 删除缓存
 *
 * @author Ray
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CacheEvict {

	/**
	 * 别名 {@link #cacheNames}.
	 *
	 * @return String[]
	 */
	@AliasFor("cacheNames")
	String[] value() default {};

	/**
	 * 缓存名称
	 *
	 * @return String[]
	 */
	@AliasFor("value")
	String[] cacheNames() default {};

	/**
	 * 缓存key，支持Beetl表达式
	 *
	 * @return String
	 */
	String key() default "";

	/**
	 * 是否删除缓存中所有数据
	 * <p>默认情况下是只删除关联key的缓存数据
	 * <p>注意：当该参数设置成 {@code true} 时 {@link #key} 参数将无效
	 *
	 * @return boolean
	 */
	boolean allEntries() default false;


}
