package com.sdnc.common.constant;

import org.springframework.http.HttpHeaders;

/**
 * 请求头常量信息
 */
public final class Header extends HttpHeaders {

	/**
	 * 操作系统
	 */
	public static final String PLATFORM_MODEL = "X-Platform-Model";

	/**
	 * 手机型号
	 */
	public static final String DEVICE_MODEL = "X-Device-Model";

	/**
	 * 系统版本号
	 */
	public static final String SYSTEM_VERSION = "X-System-Version";

	/**
	 * App版本号
	 */
	public static final String APP_VERSION = "X-App-Version";

	/**
	 * Api版本号
	 */
	public static final String API_VERSION = "X-Api-Version";

	/**
	 * 当前时间戳
	 */
	public static final String TIMESTAMP = "X-Timestamp";

	/**
	 * 访问令牌
	 */
	public static final String ACCESS_TOKEN = "X-Access-Token";

	/**
	 * 加密密钥
	 */
	public static final String AES_SIGN = "X-AES-Sign";

}
