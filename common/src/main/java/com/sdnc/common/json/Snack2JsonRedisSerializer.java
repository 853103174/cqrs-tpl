package com.sdnc.common.json;

import org.noear.snack.ONode;
import org.noear.snack.core.Feature;
import org.noear.snack.core.Options;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * 自定义Redis序列化
 */
public final class Snack2JsonRedisSerializer implements RedisSerializer<Object> {

	@Override
	public byte[] serialize(Object obj) throws SerializationException {
		Options options = Options.of(Feature.QuoteFieldNames, Feature.WriteClassName, Feature.WriteDateUseFormat);
		return ONode.stringify(obj, options).getBytes();
	}

	@Override
	public Object deserialize(byte[] bytes) throws SerializationException {
		return (bytes == null ? null : ONode.deserialize(new String(bytes)));
	}

}
